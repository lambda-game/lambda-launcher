package com.interfiber.lambda.launcher.game;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.api.VersionInfo;

import java.io.IOException;
import java.util.Objects;

public class GameFileDownloader {
    public static void downloadFilesForVersion(String version) {
        VersionInfo downloadInfo = null;
        for (VersionInfo versionInfo : LauncherSettings.versions) {
            if (Objects.equals(versionInfo.versionName, version)) {
                downloadInfo = versionInfo;
                break;
            }
        }
        System.out.println("Starting download for version: " + version);
        assert downloadInfo != null;
        Thread downloaderThread = new Thread(new InstallerRunnable(downloadInfo));
        downloaderThread.setName("Installer");
        downloaderThread.start();
    }
}
