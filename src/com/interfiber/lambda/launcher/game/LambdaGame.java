package com.interfiber.lambda.launcher.game;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.Main;

import javax.swing.*;
import java.io.File;
import java.nio.file.Paths;

public class LambdaGame {
    public static String getLauncherDir() {
        String homeDir = System.getProperty("user.home");
        String launcherDir = Paths.get(homeDir, ".lambda", "launcher").toAbsolutePath().toString();
        if (!new File(launcherDir).exists()) {
            new File(launcherDir).mkdirs();
        }
        return launcherDir;
    }

    public static String getLambdaCacheDir(){
        String homeDir = System.getProperty("user.home");
        String cacheDir = Paths.get(homeDir, ".lambda", "cache").toAbsolutePath().toString();
        if (!new File(cacheDir).exists()) {
            new File(cacheDir).mkdirs();
        }
        return cacheDir;
    }

    public static boolean hasDownloadedGameVersion(String version) {
        return Paths.get(getLauncherDir(), version + "").toFile().exists();
    }

    public static void launchGame() {
        if (LauncherSettings.isLaunching) {
            System.out.println("Already launching");
            JOptionPane.showInternalMessageDialog(null, "Lambda is updating/running. Please close it to launch again");
            return;
        }

        LauncherSettings.isLaunching = true;

        String launcherDir = getLauncherDir();
        System.out.println("Launcher directory is: " + launcherDir);
        System.out.println("Checking for game download");
        if (!hasDownloadedGameVersion(LauncherSettings.gameVersion)) {
            System.out.println("Downloading game files");
            Main.mainLauncherScreen.playButton.setText("Updating game");
            try {
                GameFileDownloader.downloadFilesForVersion(LauncherSettings.gameVersion);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Failed to download game files!");
            }
        } else {
            System.out.println("Launching game version: " + LauncherSettings.gameVersion);
            GameRunner.runGame(LauncherSettings.gameVersion, "");
        }
    }

    public static void launchServer() {
        if (!hasDownloadedGameVersion(LauncherSettings.gameVersion)){
            JOptionPane.showInternalMessageDialog(null, "Please run the game first before launching the server", "Lambda Launcher", JOptionPane.ERROR_MESSAGE);
            return;
        }

        GameRunner.runServer(LauncherSettings.gameVersion);
    }
}
