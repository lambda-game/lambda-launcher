package com.interfiber.lambda.launcher.game;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.Main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GameInstaller {
    public static void installGame(String jarPath, String version) {
        Path storageFolder = Paths.get(LambdaGame.getLauncherDir(), version);
        storageFolder.toFile().mkdirs();

        try {
            Files.move(Path.of(jarPath), Paths.get(String.valueOf(storageFolder), "lambda.jar"));
        } catch (IOException e) {
            System.out.println("Failed to copy jar");
            throw new RuntimeException(e);
        }

        System.out.println("Installed game files");
        LauncherSettings.isLaunching = false;
        Main.mainLauncherScreen.playButton.setText("Play game");
    }
}
