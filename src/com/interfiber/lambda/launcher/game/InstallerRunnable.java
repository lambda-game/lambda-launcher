package com.interfiber.lambda.launcher.game;

import com.interfiber.lambda.launcher.api.VersionInfo;
import com.interfiber.lambda.launcher.security.Sha256;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Paths;
import java.util.Objects;

public class InstallerRunnable implements Runnable {

    private final VersionInfo info;

    public InstallerRunnable(VersionInfo i) {
        info = i;
    }

    @Override
    public void run() {
        System.out.println("Downloading jar from: " + info.jarURL);
        try {
            URL downloadUrl = new URL(info.jarURL);
            ReadableByteChannel readableByteChannel = Channels.newChannel(downloadUrl.openStream());
            try (FileOutputStream fos = new FileOutputStream(Paths.get(LambdaGame.getLauncherDir(), "lambda.jar").toFile())) {
                fos.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

                System.out.println("Downloaded complete");

                System.out.println("Installing game files...");
                GameInstaller.installGame(Paths.get(LambdaGame.getLauncherDir(), "lambda.jar").toAbsolutePath().toString(), info.versionName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to download file");
        }
    }
}
