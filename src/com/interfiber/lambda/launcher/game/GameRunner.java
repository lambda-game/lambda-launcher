package com.interfiber.lambda.launcher.game;

import com.formdev.flatlaf.FlatDarkLaf;
import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.Main;
import com.interfiber.lambda.launcher.OutputWindowController;
import com.interfiber.lambda.launcher.StreamGobbler;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executors;

public class GameRunner {
    public static void runGame(String version, String extraLaunchParams) {

        String jarPath = Paths.get(LambdaGame.getLauncherDir(), version, "lambda.jar").toString();
        System.out.println("Jar file is located at: " + jarPath);

        String jvmArgs = LauncherSettings.jvmArgs;

        String OS = System.getProperty("os.name").toLowerCase();
        System.out.println("OS is: " + OS);

        if (Objects.equals(LauncherSettings.jvmArgs, "")){
            jvmArgs = "";
        }
        jvmArgs += "--add-opens=java.base/java.lang=ALL-UNNAMED --add-opens=java.base/java.math=ALL-UNNAMED --add-opens=java.base/java.util=ALL-UNNAMED --add-opens=java.base/java.util.concurrent=ALL-UNNAMED --add-opens=java.base/java.net=ALL-UNNAMED --add-opens=java.base/java.text=ALL-UNNAMED";

        if (OS.contains("mac")){
            System.out.println("Adding -XstartOnFirstThread to JVM arguments");
            if (jvmArgs == null){
                jvmArgs = "-XstartOnFirstThread";
            } else {
                jvmArgs = jvmArgs + " -XstartOnFirstThread";
            }
        }

        ArrayList<String> command = new ArrayList<>();


        command.add("java");
        command.addAll(Arrays.asList(jvmArgs.split(" ")));
        command.add("-jar");
        command.add(jarPath);

        if (LauncherSettings.enableSteamRun) {
            System.out.println("Using steam-run for launch");
            command.add(0, "steam-run");
        }

        if (Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile().exists()){
            command.add("--login-token=" + LauncherSettings.user.authToken);
        }

        command.addAll(Arrays.asList(LauncherSettings.gameArgs.split(" ")));

        command.addAll(Arrays.asList(extraLaunchParams.split(" ")));

        try {
            ProcessBuilder builder = new ProcessBuilder(command);

            Process process = builder.start();

            StreamGobbler streamGobbler =
                    new StreamGobbler(process.getInputStream(), System.out::println);

            StreamGobbler streamGobbler1 = new StreamGobbler(process.getErrorStream(), System.out::println);

            Executors.newSingleThreadExecutor().submit(streamGobbler);
            Executors.newSingleThreadExecutor().submit(streamGobbler1);

            Thread thread = new Thread(() -> {
                try {
                    int exitCode = process.waitFor();
                    if (exitCode != 0) {
                        JOptionPane.showInternalMessageDialog(null, "Lambda Crashed! Check /tmp/lambda.log for more info.", "Game Crashed", JOptionPane.ERROR_MESSAGE);
                    }
                    LauncherSettings.isLaunching = false;
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
            thread.start();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void runServer(String gameVersion) {

        File workingDir = null;

        JFileChooser dialog = new JFileChooser();
        dialog.setDialogTitle("Select server working directory");

        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (dialog.showOpenDialog(Main.mainLauncherScreen.panel) == JFileChooser.APPROVE_OPTION){
            System.out.println("Working dir is: " + dialog.getSelectedFile());
            workingDir = dialog.getSelectedFile();
        }

        String jarPath = Paths.get(LambdaGame.getLauncherDir(), gameVersion, "lambda.jar").toString();
        System.out.println("Jar file is located at: " + jarPath);

        String jvmArgs = LauncherSettings.jvmArgs;

        String OS = System.getProperty("os.name").toLowerCase();
        System.out.println("OS is: " + OS);

        if (Objects.equals(LauncherSettings.jvmArgs, "")){
            jvmArgs = null;
        }

        if (OS.contains("mac")){
            System.out.println("Adding -XstartOnFirstThread to JVM arguments");
            if (jvmArgs == null){
                jvmArgs = "-XstartOnFirstThread";
            } else {
                jvmArgs = jvmArgs + " -XstartOnFirstThread";
            }
        }

        ArrayList<String> command = new ArrayList<>();
        command.add("java");
        if (jvmArgs != null) {
            command.add(jvmArgs);
        }
        command.add("-jar");
        command.add(jarPath);
        command.add("--launch-multiplayer-server");

        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(workingDir);
            
            Process p = builder.start();

            OutputWindowController.init("Lambda server output log");

            StreamGobbler streamGobbler = new StreamGobbler(p.getInputStream(), OutputWindowController::sendLog);
            StreamGobbler streamGobbler1 = new StreamGobbler(p.getErrorStream(), OutputWindowController::sendLog);

            Executors.newSingleThreadExecutor().submit(streamGobbler);
            Executors.newSingleThreadExecutor().submit(streamGobbler1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
