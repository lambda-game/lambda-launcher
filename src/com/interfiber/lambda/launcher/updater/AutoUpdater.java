package com.interfiber.lambda.launcher.updater;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.Main;
import com.interfiber.lambda.launcher.api.ApiSettings;
import com.interfiber.lambda.launcher.api.BasicHttp;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Objects;

public class AutoUpdater {
	public void runUpdater(){
		String jarFilePath = "";
		String hash = "";
		String upstreamHash = "";

		try {
			jarFilePath = getJarFilePath();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			hash = getFileHash(jarFilePath, md);
			Main.mainLauncherScreen.launcherHash.setText("Launcher hash: " + hash);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		System.out.println("Current launcher jar hash: " + hash);
		System.out.println("Checking current hash to upstream hash");
		try {
			upstreamHash = BasicHttp.get(ApiSettings.launcherChecksumUrl);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (!hash.equals(upstreamHash)){
			Main.mainLauncherScreen.updateStatus.setText("Update status: available");
			System.out.println("Update needed, asking user");
			Object[] options = {"Update now", "Update later"};
			int n = JOptionPane.showOptionDialog(null,
					"Launcher update available! Update now?",
					"Lambda launcher auto-updater",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]);
			if (n == 0){
				System.out.println("Updating...");
				this.update(jarFilePath, upstreamHash);
			} else {
				Main.mainLauncherScreen.updateStatus.setText("Update status: canceled");
				JOptionPane.showMessageDialog(null, "Update canceled", "Lambda launcher auto-updater", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	private void update(String fileDest, String jarHash) {
		System.out.println("Downloading launcher jar...");
		try {
			String tmpLocation = ".lambdaLauncherTmp";

			AutoUpdaterDownloader.downloadFile(ApiSettings.launcherJarUrl, tmpLocation);

			System.out.println("Checking downloaded launcher hash against upstream hash");
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			String hash = getFileHash(tmpLocation, md);

			if (!jarHash.trim().equals(hash.trim())){
				System.out.println("Download hash: " + hash.trim());
				System.out.println("Upstream hash: " + jarHash.trim());
				System.out.println("Match status: " + jarHash.trim().equals(hash.trim()));
				Files.delete(Paths.get(tmpLocation));
				JOptionPane.showMessageDialog(null, "Sha256 hash mismatch error! Update canceled", "Update error", JOptionPane.ERROR_MESSAGE);
				return;
			}

			System.out.println("Copying jar into location");
			Files.delete(Paths.get(fileDest)); // remove old launcher jar
			Files.move(Paths.get(tmpLocation), Paths.get(fileDest));
			System.out.println("Update completed!");
			JOptionPane.showMessageDialog(null, "Update complete! Press any button to restart the launcher", "Update success", JOptionPane.INFORMATION_MESSAGE);

			final ArrayList<String> command = new ArrayList<>();
			command.add("java");
			command.add("-jar");
			command.add(fileDest);

			System.out.println("Spawning new launcher process");
			final ProcessBuilder builder = new ProcessBuilder(command);
			builder.start();

			System.exit(0);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String getFileHash(String filePath, MessageDigest md){
		try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filePath), md)) {
			while (dis.read() != -1) ; //empty loop to clear the data
			md = dis.getMessageDigest();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// bytes to hex
		StringBuilder result = new StringBuilder();
		for (byte b : md.digest()) {
			result.append(String.format("%02x", b));
		}
		return result.toString();
	}

	private String getJarFilePath() throws FileNotFoundException {
		String path = AutoUpdater.class.getResource(AutoUpdater.class.getSimpleName() + ".class").getFile();
		if(path.startsWith("/")) {
			throw new FileNotFoundException("This is not a jar file: \n"+path);
		}
		if(path.lastIndexOf("!")!=-1) path = path.substring(path.lastIndexOf("!/")+2, path.length());
		path = Objects.requireNonNull(ClassLoader.getSystemClassLoader().getResource(path)).getFile();

		return path.substring(0, path.lastIndexOf('!')).replace("%20", " ").replace("file:", "");
	}
}
