package com.interfiber.lambda.launcher.updater;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Paths;

public class AutoUpdaterDownloader {
	public static void downloadFile(String url, String outputFile) throws IOException {
		URL downloadUrl = new URL(url);
		try (ReadableByteChannel readableByteChannel = Channels.newChannel(downloadUrl.openStream()); FileOutputStream fos = new FileOutputStream(Paths.get(outputFile).toFile())) {
			fos.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

			System.out.println("Downloaded complete");

			System.out.println("Installing game files...");
		}
	}
}
