package com.interfiber.lambda.launcher;

import com.formdev.flatlaf.*;
import com.interfiber.lambda.launcher.api.*;
import com.interfiber.lambda.launcher.game.LambdaGame;
import com.interfiber.lambda.launcher.serverlist.ServerListHyperLinkHandler;
import com.interfiber.lambda.launcher.serverlist.ServerListLoader;
import com.interfiber.lambda.launcher.ui.MainLauncherScreen;
import com.interfiber.lambda.launcher.ui.Themes;
import com.interfiber.lambda.launcher.updater.AutoUpdater;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.nio.file.Paths;
import java.util.Objects;

public class Main {
    public static MainLauncherScreen mainLauncherScreen;

    public static void main(String[] args) {

        System.setProperty("swing.aatext", "true");
        System.setProperty("awt.useSystemAAFontSettings", "on");

        mainLauncherScreen = new MainLauncherScreen();

        System.out.println("Loading launcher settings");
        try {
            LauncherSettings.loadSettings();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to load settings");
        }

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame();
        mainLauncherScreen.newsSection.setContentType("text/html");
        mainLauncherScreen.newsSection.setEditable(false);
        mainLauncherScreen.newsSection.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
        mainLauncherScreen.newsSection.setFont(mainLauncherScreen.playButton.getFont());

        StringBuilder content = new StringBuilder();

        content.append("<html><body style='background-image: url(\"https://interfiber.srht.site/lambda/river_thunder_bg.png\"); background-repeat: no-repeat; background-position: top;  background-size: 100%;'>");
        content.append("<font size=50 color=\"#add8e6\">Lambda News!</font>\n<br><br><br>");

        for (NewsItem item : NewsApi.getNews()) {
            content.append("<br>");
            content.append("\n" + "<h1 color=\"#E6E6E3\">").append(item.title).append("</h1>");
            content.append("\n" + "<p color=\"#E6E6E3\">").append(item.content).append("</p>");
            content.append("<br>");
            content.append("\n<font color=\"white\" size=4>").append("Posted on: " + item.date).append("</font>");
            content.append("<br>");
        }

        LauncherSettings.versions = VersionApi.getVersions();
        for (VersionInfo versionInfo : Utils.reverseVersionList(LauncherSettings.versions)) {
            mainLauncherScreen.versionSelector.addItem(versionInfo.versionName);
            LauncherSettings.gameVersion = versionInfo.versionName;
            mainLauncherScreen.versionSelector.setSelectedItem(versionInfo.versionName);
        }

        mainLauncherScreen.versionSelector.addActionListener(e -> {
            System.out.println("New version is: " + mainLauncherScreen.versionSelector.getSelectedItem());
            LauncherSettings.gameVersion = Objects.requireNonNull(mainLauncherScreen.versionSelector.getSelectedItem()).toString();
        });

        mainLauncherScreen.versionSelector.setSelectedItem(VersionApi.getLatest());

        mainLauncherScreen.steamrunEnabled.addActionListener(e -> LauncherSettings.enableSteamRun = mainLauncherScreen.steamrunEnabled.isSelected());

        mainLauncherScreen.jvmArgs.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateFieldState();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateFieldState();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateFieldState();
            }

            private void updateFieldState() {
                LauncherSettings.jvmArgs = mainLauncherScreen.jvmArgs.getText();
            }
        });

        mainLauncherScreen.gameArgs.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateFieldState();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateFieldState();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateFieldState();
            }

            private void updateFieldState(){
                LauncherSettings.gameArgs = mainLauncherScreen.gameArgs.getText();
            }
        });

        mainLauncherScreen.saveSettings.addActionListener(e -> {
            System.out.println("Saving settings");
            try {
                LauncherSettings.saveSettingsToDisk();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Failed to save settings");
                throw new IllegalStateException("Failed to save settings");
            }
        });


        mainLauncherScreen.playButton.addActionListener(e -> LambdaGame.launchGame());
        mainLauncherScreen.launchServer.addActionListener(e -> LambdaGame.launchServer());


        content.append("</body></html>");
        mainLauncherScreen.newsSection.setText(content.toString());

        mainLauncherScreen.loginButton.addActionListener(e -> UserApi.loginUser(mainLauncherScreen.username.getText(), new String(mainLauncherScreen.password.getPassword())));
        mainLauncherScreen.signupButton.addActionListener(e -> UserApi.createUser(mainLauncherScreen.username.getText(), new String(mainLauncherScreen.password.getPassword())));

        try {
            UserApi.loadUserInfo();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Failed to load user info");
            JOptionPane.showInternalMessageDialog(null, "Failed to load user info", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
        }

        if (LauncherSettings.user != null) {
            mainLauncherScreen.loginUserLabel.setText("Logged in as: " + LauncherSettings.user.username);
        }
        mainLauncherScreen.signoutButton.addActionListener(e -> {
            if (Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile().exists()) {
                System.out.println("Removing login files...");
                LauncherSettings.user = null;
                mainLauncherScreen.loginUserLabel.setText("Not logged in");
                Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile().delete();
                System.out.println("Removed login file");
                JOptionPane.showInternalMessageDialog(null, "Logged out of account");
            }
        });

        Themes.init();

        for (int i = 0; i < Themes.themes.length; i++){
            FlatLaf laf = Themes.themes[i];

            mainLauncherScreen.launcherTheme.addItem(laf.getName());
        }

        mainLauncherScreen.launcherTheme.addActionListener(e -> {
            System.out.println("Setting theme to: " + mainLauncherScreen.launcherTheme.getSelectedItem());
            String selectedItem = Objects.requireNonNull(mainLauncherScreen.launcherTheme.getSelectedItem()).toString();
            try {
                UIManager.setLookAndFeel(Themes.stringToTheme(selectedItem));
                LauncherSettings.themeName = selectedItem;
                FlatLaf.updateUI();
            } catch (Exception error){
                error.printStackTrace();
                JOptionPane.showMessageDialog(null, "Failed to change theme!", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
            }
        });

        mainLauncherScreen.servers.setEditable(false);

        mainLauncherScreen.servers.addHyperlinkListener(new ServerListHyperLinkHandler());

        ServerListLoader.loadServers();

        mainLauncherScreen.reloadServers.addActionListener(e -> ServerListLoader.loadServers());

        System.out.println("Starting launcher ui");

        frame.pack();
        frame.setTitle("Lambda Launcher");
        frame.add(mainLauncherScreen.panel);
        frame.setSize(900, 900);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        FlatLaf.updateUI();

        frame.setVisible(true);

        System.out.println("Checking for updates");
        Thread thread = new Thread(() -> new AutoUpdater().runUpdater());
        thread.setName("Auto-Updater");
        thread.start();
    }
}