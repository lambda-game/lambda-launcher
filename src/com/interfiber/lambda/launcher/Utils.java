package com.interfiber.lambda.launcher;

import com.interfiber.lambda.launcher.api.VersionInfo;
import com.interfiber.lambda.launcher.mjson.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utils {
    public static ArrayList<Json> reverseArrayList(List<Json> alist) {
        // Arraylist for storing reversed elements
        ArrayList<Json> revArrayList = new ArrayList<>();
        for (int i = alist.size() - 1; i >= 0; i--) {

            // Append the elements in reverse order
            revArrayList.add(alist.get(i));
        }

        // Return the reversed arraylist
        return revArrayList;
    }

    public static ArrayList<VersionInfo> reverseVersionList(List<VersionInfo> alist) {
        // Arraylist for storing reversed elements
        ArrayList<VersionInfo> revArrayList = new ArrayList<>();
        for (int i = alist.size() - 1; i >= 0; i--) {

            // Append the elements in reverse order
            revArrayList.add(alist.get(i));
        }

        // Return the reversed arraylist
        return revArrayList;
    }
}
