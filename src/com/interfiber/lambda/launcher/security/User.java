package com.interfiber.lambda.launcher.security;

import java.io.Serializable;

public class User implements Serializable {
    public String username;
    public String authToken;
}
