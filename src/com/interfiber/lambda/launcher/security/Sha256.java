package com.interfiber.lambda.launcher.security;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;

public class Sha256 {
    public static String calc(InputStream is) {
        StringBuilder output;
        int read;
        byte[] buffer = new byte[8192];

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] hash = digest.digest();
            BigInteger bigInt = new BigInteger(1, hash);
            output = new StringBuilder(bigInt.toString(16));
            while (output.length() < 64) {
                output.insert(0, "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return output.toString();
    }

    public static String calcFile(String filename) {
        File f = new File(filename);
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f))) {
            return calc(bis);
        } catch (IOException e) {
            System.err.println(e.getClass() + ": " + e.getMessage());
        }

        return null;
    }
}
