package com.interfiber.lambda.launcher;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;
import com.interfiber.lambda.launcher.api.VersionInfo;
import com.interfiber.lambda.launcher.game.LambdaGame;
import com.interfiber.lambda.launcher.mjson.Json;
import com.interfiber.lambda.launcher.security.User;
import com.interfiber.lambda.launcher.ui.Themes;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class LauncherSettings {
    public static String gameVersion = "0.0.1";
    public static ArrayList<VersionInfo> versions;
    public static boolean isLaunching = false;
    public static boolean enableSteamRun = false;
    public static String jvmArgs = "";
    public static String gameArgs = "";
    public static User user;
    public static String themeName = "";

    public static void saveSettingsToDisk() throws IOException {
        String settingsFilePath = Paths.get(LambdaGame.getLauncherDir(), "launcher_settings.json").toString();

        if (!new File(settingsFilePath).exists()) {
            new File(settingsFilePath).createNewFile();
        }

        Json settingsJson = Json.object();
        settingsJson.set("jvmArgs", jvmArgs);
        settingsJson.set("enableSteamRun", enableSteamRun);
        settingsJson.set("gameArgs", gameArgs);
        settingsJson.set("themeName", themeName);
        String settingsJsonString = settingsJson.toString();

        FileWriter writer = new FileWriter(settingsFilePath);
        writer.write(settingsJsonString);
        writer.close();

    }

    public static void loadSettings() throws MalformedURLException {
        String settingsFilePath = Paths.get(LambdaGame.getLauncherDir(), "launcher_settings.json").toString();

        if (new File(settingsFilePath).exists()) {
            Json settingsJson = Json.read(new File(settingsFilePath).toURI().toURL());

            String jvmArguments = settingsJson.at("jvmArgs").toString();
            jvmArguments = jvmArguments.substring(1, jvmArguments.length() - 1);


            boolean steamRunEnabled = Boolean.parseBoolean(settingsJson.at("enableSteamRun").toString());

            LauncherSettings.jvmArgs = jvmArguments;
            LauncherSettings.gameArgs = settingsJson.at("gameArgs").toString().replace("\"", "");
            LauncherSettings.enableSteamRun = steamRunEnabled;
            LauncherSettings.themeName = settingsJson.at("themeName").toString().replace("\"", "");

            try {
                UIManager.setLookAndFeel(Themes.stringToTheme(LauncherSettings.themeName));
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Failed to load theme");
                System.exit(-1);
            }

            Main.mainLauncherScreen.jvmArgs.setText(LauncherSettings.jvmArgs);
            Main.mainLauncherScreen.gameArgs.setText(LauncherSettings.gameArgs);
            Main.mainLauncherScreen.steamrunEnabled.setSelected(LauncherSettings.enableSteamRun);
        } else {
            try {
                UIManager.setLookAndFeel(new FlatDarkLaf());
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Failed to load theme");
                System.exit(-1);
            }
        }

    }
}
