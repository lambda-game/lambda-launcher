package com.interfiber.lambda.launcher.serverlist;

import com.interfiber.lambda.launcher.Main;
import com.interfiber.lambda.launcher.api.ServerInfo;
import com.interfiber.lambda.launcher.api.ServerListApi;
import com.interfiber.lambda.launcher.game.LambdaGame;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.UUID;

public class ServerListLoader {
    public static void loadServers(){
        StringBuilder content = new StringBuilder();

        ArrayList<ServerInfo> serverList = ServerListApi.getServers();

        content.append("<html><body style=\"background-image: url('https://git.sr.ht/~interfiber/lambda/blob/main/assets/cave_wall_stone.png');\">");
        content.append("<font size=50 color=\"#add8e6\">Lambda public server list</font>\n<br><br><br>");

        for (ServerInfo info : serverList){

            String strTmp = System.getProperty("java.io.tmpdir");
            String path = Paths.get(LambdaGame.getLambdaCacheDir(), UUID.randomUUID() + ".serverIcon.png").toString();
            System.out.println("Image cache file is at: " + path);

            if (!new File(path).exists()){
                System.out.println("Loading image file...");

                byte[] imageData = Base64.getDecoder().decode(info.serverIcon.substring(info.serverIcon.indexOf(",") + 1));

                try {
                    BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageData));
                    ImageIO.write(bufferedImage, "png", new File(path));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            content.append("<br>");
            content.append("\n" + "<img src=").append(new File(path).toURI()).append("></img>");
            content.append("\n" + "<h1 color=\"#E6E6E3\">").append(info.serverName).append("</h1>");
            content.append("\n" + "<p color=\"#E6E6E3\">").append(info.serverDesc).append("</p>");
            content.append("\n<br>");
            content.append("\n<a color=\"#aaaaff\" style=\"font-size: 22;\" href=\"lambdajoin://").append(info.serverIp).append("\">").append("Connect to server: ").append(info.serverIp).append("</a>");
            content.append("<br>");
        }

        content.append("</body>\n</html>");

        Main.mainLauncherScreen.servers.setContentType("text/html");
        Main.mainLauncherScreen.servers.setText(content.toString());
    }
}
