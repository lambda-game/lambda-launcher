package com.interfiber.lambda.launcher.serverlist;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.game.GameRunner;

import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class ServerListHyperLinkHandler implements HyperlinkListener {
    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {

            String u = e.getDescription();

            if (u.startsWith("lambdajoin://")) {
                // launch lambda
                GameRunner.runGame(LauncherSettings.gameVersion, "--join-server=" + u.replace("lambdajoin://", ""));
            }
        }
    }
}
