package com.interfiber.lambda.launcher.ui;

import com.formdev.flatlaf.*;

import javax.swing.*;
import java.util.Objects;

public class Themes {
    public static FlatLaf[] themes;

    public static void init(){
        themes = new FlatLaf[]{new FlatLightLaf(), new FlatDarkLaf(), new FlatIntelliJLaf(), new FlatDarculaLaf()};
   }

   public static FlatLaf stringToTheme(String selectedItem){
       if (Objects.equals(selectedItem, "FlatLaf Dark")) {
           return new FlatDarkLaf();
       } else if (Objects.equals(selectedItem, "FlatLaf Light")) {
           return new FlatLightLaf();
       } else if (Objects.equals(selectedItem, "FlatLaf IntelliJ")) {
           return new FlatIntelliJLaf();
       } else if (Objects.equals(selectedItem, "FlatLaf Darcula")) {
           return new FlatDarculaLaf();
       } else {
           return null;
       }
   }
}
