package com.interfiber.lambda.launcher;

import com.interfiber.lambda.launcher.ui.OutputViewer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class OutputWindowController {

    private static OutputViewer outputViewer;

    public static void init(String logTitle){
        outputViewer = new OutputViewer();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        outputViewer.title.setText(logTitle);

        outputViewer.clearLog.addActionListener(e -> {
            outputViewer.logArea.setText("");
            outputViewer.lineCount.setText("Lines: 0");
        });

        outputViewer.closeWindow.addActionListener(e -> frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)));

        frame.add(outputViewer.pane);
        frame.setSize(800, 800);
        frame.setVisible(true);
    }

    public static void sendLog(String message){
        outputViewer.logArea.append(message + "\n");
        outputViewer.lineCount.setText("Lines: " + outputViewer.logArea.getLineCount());
    }
}
