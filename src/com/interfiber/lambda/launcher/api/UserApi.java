package com.interfiber.lambda.launcher.api;

import com.interfiber.lambda.launcher.LauncherSettings;
import com.interfiber.lambda.launcher.Main;
import com.interfiber.lambda.launcher.game.LambdaGame;
import com.interfiber.lambda.launcher.mjson.Json;
import com.interfiber.lambda.launcher.security.User;

import javax.swing.*;
import java.io.*;
import java.nio.file.Paths;

public class UserApi {
    public static void loginUser(String username, String password){
        String userUrl = ApiSettings.authServer + "/api/v1/auth/users/authenticateFromCreds";

        System.out.println("Logging in...");

        Json requestPayload = Json.object();
        requestPayload.set("username", username);
        requestPayload.set("password", password);

        String response = null;
        try {
            response = BasicHttp.post(userUrl, requestPayload.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to fetch user info from authserver");
            System.exit(-1);
        }

        // parse
        Json userJson = Json.read(response);

        if (userJson.at("user").toString().replace("\"", "").equals("null")){
            System.out.println("Invalid login");
            JOptionPane.showInternalMessageDialog(null, "Invalid username, or password", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
            return;
        }

        User user = new User();
        user.authToken = userJson.at("user").at("authToken").toString().replace("\"", "");
        user.username = userJson.at("user").at("username").toString().replace("\"", "");

        System.out.println("Saving user info to disk");
        try {
            FileOutputStream fileInputStream = new FileOutputStream(Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile());
            ObjectOutputStream obj = new ObjectOutputStream(fileInputStream);
            obj.writeObject(user);
            obj.close();
            fileInputStream.close();
            JOptionPane.showInternalMessageDialog(null, "You have logged into lambda");
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Failed to save user info to disk");
        }

        try {
            loadUserInfo();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to load user data loginUser(...);");
        }
        Main.mainLauncherScreen.loginUserLabel.setText("Logged in as: " + LauncherSettings.user.username);
    }

    public static void createUser(String username, String password) {
        String userUrl = ApiSettings.authServer + "/api/v1/auth/users/register";

        System.out.println("Registering user...");

        Json requestPayload = Json.object();
        requestPayload.set("username", username);
        requestPayload.set("password", password);

        String response = null;
        try {
            response = BasicHttp.post(userUrl, requestPayload.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to create user with authserver");
            System.exit(-1);
        }

        Json responseJson = Json.read(response);
        String status = responseJson.at("status").toString().replace("\"", "");

        if (status.equals("CREATED_USER")){
            JOptionPane.showInternalMessageDialog(null, "Registered user! Please sign in");
        } else if (status.equals("TAKEN_USERNAME")) {
            JOptionPane.showInternalMessageDialog(null, "Username already taken", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
        } else if (status.equals("RATELIMITED")){
            JOptionPane.showInternalMessageDialog(null, "Ratelimited, please wait 2 mins and try again.", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
        } else if (status.equals("DISALLOWED_PASSWORD") || status.equals("DISALLOWED_USERNAME")){
            JOptionPane.showInternalMessageDialog(null, "You cannot use that username, or password. Please choose another", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showInternalMessageDialog(null, "Invalid server response", "Lambda launcher", JOptionPane.ERROR_MESSAGE);
        }

    }

    public static void loadUserInfo() throws IOException, ClassNotFoundException {
        if (Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile().exists()) {
            System.out.println("Loading user info");
            FileInputStream fi = new FileInputStream(Paths.get(LambdaGame.getLauncherDir(), "loginInfo.dump").toFile());
            ObjectInputStream oi = new ObjectInputStream(fi);

            LauncherSettings.user = (User) oi.readObject();

            fi.close();
            oi.close();

            System.out.println("Loaded user info");
        }
    }
}
