package com.interfiber.lambda.launcher.api;

public class NewsItem {
    public String title;
    public String date;
    public String content;
}
