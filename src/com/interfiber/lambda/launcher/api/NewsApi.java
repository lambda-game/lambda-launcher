package com.interfiber.lambda.launcher.api;

import com.interfiber.lambda.launcher.Utils;
import com.interfiber.lambda.launcher.mjson.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewsApi {
    public static ArrayList<NewsItem> getNews() {
        System.out.println("Fetching news from api server");
        String newsUrl = ApiSettings.gateway + "/api/v1/news/getNews";

        String newsContent = null;
        try {
            newsContent = BasicHttp.get(newsUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to fetch news");
        }

        Json news = Json.read(newsContent);
        List<Json> newsList = news.asJsonList();
        ArrayList<NewsItem> newsItems = new ArrayList<>();

        for (Json newsItem : Utils.reverseArrayList(newsList)) {
            NewsItem item = new NewsItem();
            item.content = newsItem.at("content").toString().substring(1).replace("\"", "");
            item.date = newsItem.at("date").toString().substring(1).replace("\"", "");
            item.title = newsItem.at("title").toString().substring(1).replace("\"", "");

            newsItems.add(item);
        }

        return newsItems;
    }
}
