package com.interfiber.lambda.launcher.api;

public class VersionInfo {
    public String jarURL;
    public String hash;
    public String versionName;
    public int javaVersion;
    public boolean isLatestVersion = false;
}
