package com.interfiber.lambda.launcher.api;

public class ServerInfo {
    public String serverName;
    public String serverDesc;
    public String serverIcon;
    public String serverIp;
}