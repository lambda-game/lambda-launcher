package com.interfiber.lambda.launcher.api;

public class ApiSettings {
    public static String gateway = "https://lambda-update-server.onrender.com";
    public static String authServer = "https://lambda-auth.onrender.com";
    public static String launcherChecksumUrl = "https://gitlab.com/lambda-game/lambda-versions/-/raw/main/launcher.sha256";
    public static String launcherJarUrl = "https://gitlab.com/lambda-game/lambda-versions/-/raw/main/launcher.jar";
}
