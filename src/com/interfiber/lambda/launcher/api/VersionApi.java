package com.interfiber.lambda.launcher.api;

import com.interfiber.lambda.launcher.mjson.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class VersionApi {
    public static ArrayList<VersionInfo> getVersions() {
        System.out.println("Fetching versions from api server");
        String versionUrl = ApiSettings.gateway + "/api/v1/versions/list";

        String versionContent;
        try {
            versionContent = BasicHttp.get(versionUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to fetch versions");
        }

        Json versions = Json.read(versionContent);
        List<Json> versionList = versions.asJsonList();
        ArrayList<VersionInfo> versionInfoList = new ArrayList<>();
//        String latestVersion = versions.at("latest").toString().substring(1).replace("\"", "");

        for (Json json : versionList) {

            VersionInfo info = new VersionInfo();
            info.jarURL = json.at("jarUrl").toString().substring(1).replace("\"", "");
            info.versionName = json.at("versionName").toString().substring(1).replace("\"", "");
//            if (Objects.equals(info.versionName, latestVersion)) {
//                info.isLatestVersion = true;
//            }
            versionInfoList.add(info);
        }

        return versionInfoList;

    }

    public static String getLatest(){
        System.out.println("Fetching latest version from api server");
        String versionUrl = ApiSettings.gateway + "/api/v1/versions/latest";

        String versionContent;
        try {
            versionContent = BasicHttp.get(versionUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to fetch versions");
        }

        Json latestJson = Json.read(versionContent);

        return latestJson.at("latest").toString().substring(1).replace("\"", "");
    }
}
