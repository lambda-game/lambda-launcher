package com.interfiber.lambda.launcher.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class BasicHttp {
    public static String get(String url) throws IOException {
        URL webUrl = new URL(url);

        HttpURLConnection connection = (HttpURLConnection) webUrl.openConnection();

        InputStream responseStream = connection.getInputStream();
        return new String(responseStream.readAllBytes());
    }

    public static String post(String url, String body) throws IOException {
        URL webUrl = new URL(url);
        URLConnection con = webUrl.openConnection();
        HttpURLConnection http = (HttpURLConnection)con;
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.setDoOutput(true);
        http.connect();
        try(OutputStream os = http.getOutputStream()) {
            os.write(body.getBytes());
        }

        InputStream responseStream = con.getInputStream();
        return new String(responseStream.readAllBytes());
    }
}
