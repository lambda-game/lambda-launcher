package com.interfiber.lambda.launcher.api;

import com.interfiber.lambda.launcher.mjson.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServerListApi {
    public static ArrayList<ServerInfo> getServers() {
        System.out.println("Fetching server list from api server");
        String serverListUrl = ApiSettings.gateway + "/api/v1/serverlist/list";

        String serverListContent = null;

        try {
            serverListContent = BasicHttp.get(serverListUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to fetch servers");
        }

        Json servers = Json.read(serverListContent);
        List<Json> serverListJson = servers.asJsonList();
        ArrayList<ServerInfo> serverList = new ArrayList<>();
        Collections.shuffle(serverListJson);

        for (Json json : serverListJson){
            ServerInfo info = new ServerInfo();
            info.serverDesc = json.at("serverDesc").toString().substring(1).replace("\"", "");
            info.serverIcon = json.at("serverIcon").toString().substring(1).replace("\"", "");
            info.serverIp = json.at("serverIp").toString().substring(1).replace("\"", "");
            info.serverName = json.at("serverName").toString().substring(1).replace("\"", "");

            serverList.add(info);
        }

        return serverList;
    }
}
