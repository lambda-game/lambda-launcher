# Lambda Launcher v2.0

## MacOS support
The launcher automagically allows lambda to run on MacOS by adding the -XstartOnFirstThread JVM argument.

## Screenshots
![](./screenshot_1.png)